module.exports = {
  outDir: {
    dev: './dev',
    release: './release'
  },
  server: {
    host: 'localhost',
    port: 3000
  },
  vendor: {
    css: [
      './src/styles/vendor/*.css'
    ],
    js: [
      'bower_components/video.js/dist/video.min.js',
      'bower_components/q/q.js',
      'bower_components/lodash/dist/lodash.min.js'
    ]
  },
  app_files: {
    index: './src/index.html',
    assets: [
      './src/assets/**/*.tpl.html',
      './src/assets/**/*.png',
      './src/assets/**/*.json',
      './src/assets/**/*.{ttf,woff,eot,svg}'
    ],
    css: './src/**/styles.styl',
    watchCSS: './src/**/*.styl',
    js: './src/**/*.js'
  }
};