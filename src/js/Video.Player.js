(function (container, _) {
  'use strict';
  container.VideoModule = container.VideoModule || {};

  function Player(options) {
    this.modules = {
      connector: new container.VideoModule.Connector()
    };

    var instance = this;
    var element = document.createElement('div');
    element.$$instance = instance;
    element.className = 'player';

    instance.init = function (parent) {
      parent.insertBefore(element, parent.firstChild);
    };

    instance.showVideo = function (data) {
      instance.modules.connector.$getTemplate(options.templateUrl)
        .then(function (template) {
          element.innerHTML = '';
          element.innerHTML = _.template(template)({data: data});
          videojs(element.querySelector('.video-js'));
        });
    };
  }

  container.VideoModule.Player = Player;
})(window, _);