(function (container, Q) {
  'use strict';
  container.VideoModule = container.VideoModule || {};

  var instance;

  function Connector () {
    if (instance) { return instance; }
    instance = this;
    instance.constructor = Connector;
  }

  Connector.prototype = {
    $cache: {},

    $getCache: function () {
      return this.$cache;
    },

    $isInCache: function (type, url, dataType) {
      var cache = this.$getCache();
      if (cache[type] && cache[type][dataType] && cache[type][dataType][url]) {
        return true;
      }
      return false;
    },

    $setInCache: function (type, url, dataType, data) {
      var cache = this.$getCache();
      cache[type] = cache[type] || {};
      cache[type][dataType] = cache[type][dataType] || {};
      cache[type][dataType][url] = data;
      return cache;
    },

    $getFromCache: function (type, url, dataType) {
      var cache = this.$getCache();
      return cache[type][dataType][url];
    },

    $getTemplate: function (url) {
      return this.$get(url, 'text/template');
    },

    $getJSON: function (url) {
      return this.$get(url, 'application/json');
    },

    $get: function (url, dataType) {
      if (this.$isInCache('GET', url, dataType)) {
        return Q.resolve(this.$getFromCache('GET', url, dataType));
      }

      return this.$request('GET', url, dataType);
    },

    $request: function (type, url, dataType) {
      var request = new XMLHttpRequest();
      var deferred = Q.defer();
      var self = this;

      if (request.overrideMimeType) {
        request.overrideMimeType(dataType);
      }

      request.open(type, url, true);
      request.onload = onload;
      request.onerror = onerror;
      request.send();

      function onload () {
        if (request.status === 200) {
          deferred.resolve(request.responseText);
          self.$setInCache(type, url, dataType, request.responseText);
        } else {
          deferred.reject(request);
        }
      }

      function onerror() {
        deferred.reject(new Error("Can't XHR " + JSON.stringify(url)));
      }

      return deferred.promise;
    }
  };

  container.VideoModule.Connector = Connector;
})(window, Q);