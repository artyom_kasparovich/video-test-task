(function (container, _) {
  'use strict';
  container.VideoModule = container.VideoModule || {};

  function List(options) {
    this.modules = {
      connector: new container.VideoModule.Connector()
    };
    var instance = this;
    var element = document.createElement('ul');
    element.className = 'video-list';
    element.$$instance = instance;
    instance.elements = [];

    function init(data, parent) {
      instance.modules.connector.$getTemplate(options.templateUrl)
        .then(function (template) {
          data.forEach(function (obj) {
            var elem = new container.VideoModule.ListItem();
            elem.render(element, obj, template);
            instance.elements.push(elem);
          });

          parent.appendChild(element);
          instance.setActive(data.shift());
          instance.elements[0].setActive();
        });
    }

    instance.setActive = function (data) {
      element.parentNode.$$instance.setActive(data);
    };

    instance.unsetActive = function () {
      instance.elements.forEach(function (element) {
        element.unsetActive();
      });
    };

    instance.init = init;
  }

  container.VideoModule.List = List;
})(window, _);