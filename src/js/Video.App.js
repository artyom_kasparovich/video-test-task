(function (container) {
  'use strict';
  container.VideoModule = container.VideoModule || {};

  function App(options) {
    if (!options) {
      throw new Error('Please provide options');
    }

    this.options = options.app;

    this.modules = {
      connector: new container.VideoModule.Connector(),
      list: new container.VideoModule.List(options.listItem),
      player: new container.VideoModule.Player(options.player)
    };

    var instance = this;
    var element = document.getElementById(this.options.id);
    element.$$instance = instance;

    function init() {
      instance.modules.connector.$getJSON(instance.options.url)
        .then(function (result) {
          var data = JSON.parse(result);
          instance.modules.list.init(data, element);
          instance.modules.player.init(element);
        })
        .catch(function (err) {
          element.appendChild(instance.createError(err));
        });
    }

    instance.init = init;

    instance.setActive = function (data) {
      instance.modules.player.showVideo(data);
    };

    instance.createError = function (err) {
      var element = document.createElement('div');
      element.className = 'error-block';
      element.innerHTML = '<p class="error-block__title"> Error </p>' +
        '<i class="error-block__url">' + err.responseURL + '</i>' +
        '<p class="error-block__message">' + err.status + ' - ' + err.statusText + '</p>' +
        '<p class="error-block__body"> We could not find the above request on our server </p>';
      return element;
    };
  }

  container.VideoModule.App = App;
})(window);