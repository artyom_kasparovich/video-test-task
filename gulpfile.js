var gulp = require('gulp');
var stylus = require('gulp-stylus');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglifyJS = require('gulp-uglify');
var uglifyCSS = require('gulp-clean-css');
var webserver = require('gulp-webserver');
var clean = require('gulp-clean');
var inject = require('gulp-inject');
var series = require('stream-series');
var config = require('./config.js');


function AppBuilder(config) {
  this.config = config;

  this.copyToDirectory = function (stream, directory) {
    return stream.pipe(gulp.dest(directory));
  };

  this.getAssetsStream = function () {
    return gulp.src(this.config.app_files.assets);
  };

  this.getCSSStream = function () {
    return gulp.src(this.config.app_files.css)
      .pipe(stylus())
      .pipe(autoprefixer({
        browsers: ['last 3 versions'],
        cascade: true
      }));
  };

  this.getVendorCSSStream = function () {
    return gulp.src(this.config.vendor.css);
  };

  this.getMinifyCSSStream = function () {
    return series(this.getVendorCSSStream(), this.getCSSStream())
      .pipe(concat('styles.css'))
      .pipe(uglifyCSS({compability: 'ie9'}));
  };

  this.getJSStream = function () {
    return gulp.src(this.config.app_files.js);
  };

  this.getVendorJSStream = function () {
    return gulp.src(this.config.vendor.js);
  };

  this.getMinifyJSStream = function () {
    return series(this.getVendorJSStream(), this.getJSStream())
      .pipe(concat('scripts.js'))
      .pipe(uglifyJS());
  };

  this.processIndexHTMLForDev = function () {
    return gulp.src(this.config.app_files.index)

      .pipe(inject(series(this.getVendorCSSStream(), this.getCSSStream()), {
        relative: true,
        transform: function (filepath, file, i, length) {
          var name = filepath.split('/').pop();
          return '<link rel="stylesheet" href="styles/' + name + '"/>';
        }
      }))

      .pipe(inject(series(this.getVendorJSStream(), this.getJSStream()), {
        relative: true,
        transform: function (filepath, file, i, length) {
          var name = filepath.split('/').pop();
          return '<script src="js/'+ name +'"></script>';
        }
      }))
      .pipe(gulp.dest(this.config.outDir.dev));
  };

  this.processIndexHTMLForRelease = function () {
    return gulp.src(this.config.app_files.index)

      .pipe(inject(this.getMinifyCSSStream(), {
        relative: true,
        transform: function (filepath, file, i, length) {
          var name = filepath.split('/').pop();
          return '<link rel="stylesheet" href="styles/' + name + '"/>';
        }
      }))

      .pipe(inject(this.getMinifyJSStream(), {
        relative: true,
        transform: function (filepath, file, i, length) {
          var name = filepath.split('/').pop();
          return '<script src="js/'+ name +'"></script>';
        }
      }))
      .pipe(gulp.dest(this.config.outDir.release));
  };

  this.watch = function (directory) {
    var self = this;

    gulp.watch(this.config.app_files.watchCSS, function () {
      self.copyToDirectory(self.getCSSStream(), directory);
    });

    gulp.watch(this.config.app_files.js, function () {
      self.copyToDirectory(self.getJSStream(), directory);
    });

    gulp.watch(this.config.app_files.index, function () {
      self.processIndexHTMLForDev();
    });
  };

  this.runDev = function (directory) {
    /**
     * Move assets files
     */
    this.copyToDirectory(this.getAssetsStream(), directory + '/assets');

    /**
     * Move CSS files
     */
    this.copyToDirectory(this.getVendorCSSStream(), directory + '/styles');
    this.copyToDirectory(this.getCSSStream(), directory);

    /**
     * Move JS files
     */
    this.copyToDirectory(this.getVendorJSStream(), directory + '/js');
    this.copyToDirectory(this.getJSStream(), directory);

    /**
     * Move and process index.html
     */
    this.processIndexHTMLForDev();
  };

  this.runRelease = function (directory) {
    /**
     * Move assets files
     */
    this.copyToDirectory(this.getAssetsStream(), directory + '/assets');

    /**
     * Move CSS files
     */
    this.copyToDirectory(this.getMinifyCSSStream(), directory + '/styles');

    /**
     * Move JS files
     */
    this.copyToDirectory(this.getMinifyJSStream(), directory + '/js');

    /**
     * Move and process index.html
     */
    this.processIndexHTMLForRelease();
  };

  this.startLocalServer = function (path) {
    gulp.src(path)
      .pipe(webserver({
        host: this.config.host || 'localhost',
        port: this.config.port || 3000
      }));
  };

  this.clean = function () {
    return gulp.src([this.config.outDir.dev, this.config.outDir.release], {read: false})
      .pipe(clean());
  };
};

var appBuilder = new AppBuilder(config);

gulp.task('default', function () {
  appBuilder.runDev(appBuilder.config.outDir.dev);
  appBuilder.watch(appBuilder.config.outDir.dev);
  appBuilder.startLocalServer(appBuilder.config.outDir.dev);
});

gulp.task('release', function () {
  appBuilder.runRelease(appBuilder.config.outDir.release);
});

gulp.task('server', function () {
  appBuilder.startLocalServer(appBuilder.config.outDir.release);
});

gulp.task('clean', function () {
  appBuilder.clean();
});